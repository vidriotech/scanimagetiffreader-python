Python Interface
~~~~~~~~~~~~~~~~

About
-----

.. automodule:: ScanImageTiffReader

API
---

.. autoclass:: ScanImageTiffReader
    :members:

Changelog
---------

.. table::

    ================ ===========================================================
    Version          Highlights
    ================ ===========================================================
    1.4              * Support for loading a selected interval of frames
    1.3              * Python 3.6 support.
                     * Made pip installable.
    1.2              * Fix: Properly return error messages to caller.
    1.0              * Initial release
    ================ ===========================================================